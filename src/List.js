import React from 'react';
import datasource from './datasource';
import EditList from './EditList';
import { Navigate, Link } from 'react-router-dom';


class List extends React.Component{

    constructor(props) {
        super(props);
        this.state = {
            listid : this.props.listid,
            userid : this.props.userid,
            listname: this.props.name,
            deleteResponse: "",
        }
    }

    handleEditList(e){
        console.log("Handle list");
        e.preventDefault();
    }

    handleDeleteList = async(e) =>{
        e.preventDefault();
        const response = datasource.delete('/lists/' + this.state.listid);
        console.log(response);
        this.setState({deleteResponse: response.message});
        alert(this.state.deleteResponse);
    }

    render () {    
        return(
            <tr>
                <td>{this.state.listname}</td>
                <td>
                    <button onClick={this.handleEditList}>Edit</button>
                </td>
                <td>
                    <form onSubmit={this.handleDeleteList}>
                        <input type="submit" value="Delete" />
                    </form>
                </td>
            </tr>
        );
    }
}

export default List;