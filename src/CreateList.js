import React from 'react';
import datasource from './datasource';

class CreateList extends React.Component{


    constructor(props){
        super(props);
        this.state = {
            listname: "",
            userid: this.props.userid
        }
        this.handleName = this.handleName.bind(this);
    }

    handleCreateList = async(e) => {
        const list = {
            "userid" : this.state.userid,
            "listname" : this.state.listname
        }
        console.log(list);
        const response = await datasource.post('/lists', list);
        console.log(response);
        alert(response.message);
    }

    handleName(e){
        this.setState({listname: e.target.value});
    }

    render(){
        return(
            <div>
                <h3>createlist</h3>
                <form onSubmit={this.handleCreateList}>
                    <div className="form-control">
                        <label>Name:</label>
                        <input type="text" name="name" id="name" className="form-control" value={this.state.listname} onChange={this.handleName} />
                    </div>
                    <input type="submit" value="Create List" />
                </form>
            </div>
        );
    }

}

export default CreateList;