import React from 'react';
import datasource from './datasource';
import { BrowserRouter, Route, Router, Routes, Navigate} from 'react-router-dom';
import LoginPage from './Login';
import Register  from './Register';
import DisplayLists from './DisplayLists';
import EditList from './EditList';
import CreateList from './CreateList';

export default class App extends React.Component {

    state = { userid: -1, lists: [], listsready: false};

    handleLogin = async (authenticated) => {
        this.setState({userid: authenticated});
        if(authenticated > -1){
            const lists = await datasource.get('/lists/' + authenticated);
            this.setState({lists: lists, listsready: true});
        }    
    }


    render () {
        return (
            <BrowserRouter>
                <Routes>
                    <Route path="/login" element={<LoginPage onClick={this.handleLogin} />} />
                    <Route path="/register" element={<Register />} />
                    <Route path="/lists/:userid" element={<DisplayLists/>} />
                    <Route path="/editlist/:listid/:listname" element={<EditList />} />
                    <Route path="/createlist/:userid" element={<CreateList />} />
                </Routes>
                {this.state.listsready ? <DisplayLists lists={this.state.lists} userid={this.state.userid} /> : <div></div>}
            </BrowserRouter>
        )
    }

}