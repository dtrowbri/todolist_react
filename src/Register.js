import React from 'react';
import datasource from './datasource';


class Register extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            username: "",
            emailaddress: "",
            password: "",
            verifypassword: "",
            passwordsMatch: false,
            responseMessage: ""
        }

        this.handleRegistration = this.handleRegistration.bind(this);
        this.handlePassword = this.handlePassword.bind(this);
        this.handleUsername = this.handleUsername.bind(this);
        this.handleVerifyPassword = this.handleVerifyPassword.bind(this);
        this.handleEmail = this.handleEmail.bind(this);
    }


    handleRegistration = async(e) => {
        console.log(this.state);
        e.preventDefault();
        if(this.state.password === this.state.verifypassword){
            this.setState({passwordsMatch: true});
            const user = {
                username: this.state.username,
                password: this.state.password,
                emailaddress: this.state.emailaddress
            }
            const response = await datasource.post('/users', user);
            this.setState({responseMessage: response.data.message});
        } else {
            this.setState({passwordsMatch: false});
        }
    }

    handleUsername(e){
        this.setState({username: e.target.value});
    }

    handleEmail(e) {
        this.setState({emailaddress: e.target.value});
    }

    handlePassword(e) {
        this.setState({password: e.target.value});
    }

    handleVerifyPassword(e) {
        this.setState({verifypassword: e.target.value});
    }

    render () {
        return (
            <div>
                <h3>Register</h3>
                {this.state.responseMessage ? <p>{this.state.responseMessage}</p>: <div></div>}
                <form onSubmit={this.handleRegistration}>
                    <div className="form-group">
                        <label>Username:</label>
                        <input type="text" className="form-control" id="username" name="username" value={this.state.username} onChange={this.handleUsername}/>
                    </div>
                    <div className="form-group">
                        <label>Email:</label>
                        <input type="text" className="form-control" id="email"  name="email" value={this.state.emailaddress} onChange={this.handleEmail}/>
                    </div>
                    <div className="form-group">
                        <label>Password:</label>
                        <input type="password" className="form-control" id="password"  name="password" value={this.state.password} onChange={this.handlePassword}/>
                    </div>
                    <div className="form-group">
                        {this.state.passwordsMatch ? <p>Passwords do not match</p>: <div></div>}
                        <label>Verify Password:</label>
                        <input type="password" className="form-control" id="verifyPassword"  name="verifyPassword" value={this.state.verifypassword} onChange={this.handleVerifyPassword}/>
                    </div>
                    <button className="btn btn-primary">Register</button>
                </form>
            </div>
        );
    }
}

export default Register;