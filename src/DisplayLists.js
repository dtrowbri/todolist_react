import React from 'react';
import datasource from './datasource';
import {Link} from 'react-router-dom';
import List from './List';

class DisplayLists extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            lists: this.props.lists.data,
            userid: this.props.userid
        };
    }

    render() {
        let keycounter = 0;
        const listRows = this.state.lists.map(
            (list) => {
                return (
                    <List key={keycounter++} name={list.listname} userid={this.state.userid} listid={list.listid} />
                )
            }
        )

        return (
            <div>
                <Link to={"/createlist/" + this.state.userid} state={{userid: this.state.userid}} >Create List</Link>
                <table>
                    <thead>
                        <th>List</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </thead>
                    <tbody>
                        {listRows}
                    </tbody>
                </table>
            </div>

        );
    }

}

export default DisplayLists;