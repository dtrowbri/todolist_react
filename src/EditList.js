import React from 'react';

class EditList extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            listname: this.props.name,
            listid: this.props.listid,
            userid: this.props.userid  
        };
        console.log(this.state);
        console.log(this.props);
        console.log(this.location.state.listid);
    }

    handleEditList(){

    }

    handleName(e){
        this.setState({listname: e.target.value});
    }

    render () {

        return (
            <div>
                <form onSubmit={this.handleEditList}>
                <div class="form-group">
                    <p htmlFor="name">Name:</p>
                    <input class="form-control" id="name" name="name" onChange={this.handleName} value={this.state.listname}/>
                </div>
                    <button class="btn btn-primary">Edit List</button>
                </form>
            </div>
        );
    }
}

export default EditList;