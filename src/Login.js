import React from 'react';
import datasource from './datasource';

class LoginPage extends React.Component {

    constructor(props) {
        super(props);
        this.state = { userid: -1, username: "", password: ""};

        this.handleLogin = this.handleLogin.bind(this);
        this.handleUsernameChange = this.handleUsernameChange.bind(this);
        this.handlePasswordChange = this.handlePasswordChange.bind(this);
    }

    

    handleLogin = async (e) => {
        e.preventDefault();
        const user = {
            username: this.state.username,
            password: this.state.password,
            emailaddress: ""
        };
        const response = await datasource.post('/authenticate', user);
        this.setState({userid: response.data.userid}, () =>
            this.props.onClick(this.state.userid));

    }

    handleUsernameChange(e) {
        this.setState({username: e.target.value});   
    }

    handlePasswordChange(e) {
        this.setState({password: e.target.value});
    }

    render() {
        return (
            <div>
                <h3>Login</h3>
                <form onSubmit={this.handleLogin}>
                    <div className="from-group">
                        <label htmlFor="username">Username:</label>
                        <input type="text" className="form-control" id="username" name="username" value={this.state.username} onChange={this.handleUsernameChange} />
                    </div>
                    <div className="form-group">
                        <label htmlFor="password">Password:</label>
                        <input type="password" className="form-control" id="password" name="password" value={this.state.password} onChange={this.handlePasswordChange} />
                    </div>
                    <button className="btn btn-primary">Login</button>
                </form>
            </div>
        );
    }
}

export default LoginPage;